export default class ChuckNorrisService {

    _apiBaseUrl = 'https://api.chucknorris.io'

    getResource = async (url) => {
        const res = await fetch(`${this._apiBaseUrl}${url}`)
        const body = await res.json()

        return body
    }


    getChuckNorris = async () => {
        const res = await this.getResource(`/jokes/random`)

        return res
    }


}



