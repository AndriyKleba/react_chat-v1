import React from "react";
import MessageOwner from "../MessageOwner/MessageOwner";
import MessagePartner from "../MessagePartner";

import "./MessagePanel.css";

export default class MessagePanel extends React.Component {


    render() {
        const {dialogs} = this.props
        // const {items} = this.props

        const messages = dialogs.map((message, idx) => {
            const {id, avatar, value, created_at} = message
            if (message.isMe) {
                return (
                    <MessageOwner
                        key={idx}
                        id={id}
                        avatar={avatar}
                        value={value}
                        created_at={created_at}
                    />
                )
            } else {
                return (
                    <MessagePartner
                        key={idx}
                        id={id}
                        avatar={avatar}
                        value={value}
                        created_at={created_at}
                    />
                )
            }
        })

        // const itemMessage = items.map(item => item.dialogs.messages)
        // console.log(itemMessage)


        return (
            <div className="chat__main-dialog-middle-chat">
                {messages}
            </div>
        )
    }
}