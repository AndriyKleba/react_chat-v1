import formatDistanceToNow from "date-fns/formatDistanceToNow";
import uaLocale from "date-fns/locale/uk";

const Time = ({date}) => formatDistanceToNow(date, {
    addSuffix: true,
    locale: uaLocale
});

export default Time;