import React from "react";
import Time from "../Time";

import './Message.css'

export default class Message extends React.Component {

    render() {
        const {avatar, text, date, isMe} = this.props
        const message = isMe ? 'messageIsMe' : 'message';

        return (
            <div className={message}>
                <div className={`${message}__avatar`}>
                    <img src={avatar} alt="User Avatar"/>
                </div>
                <div className={`${message}__content`}>
                    <div className={`${message}__information`}>
                        <p className={`${message}__text`}>{text}</p>
                    </div>
                    <span className={`${message}__date`}>{<Time date={date}/>}</span>
                </div>
            </div>
        )
    }
}


