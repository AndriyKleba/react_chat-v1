import React from "react";
import Time from "../Time";

import './MessagePartner.css'

export default class MessagePartner extends React.Component {

    render() {
        const {avatar, created_at, value} = this.props

        return (
            <div className="message">
                <div className="message__avatar">
                    <img src={avatar} alt="User Avatar"/>
                </div>
                <div className="message__content">
                    <div className="message__information">
                        <p className="message__text">{value}</p>
                    </div>
                    <span className="message__date">{<Time date={created_at}/>}</span>
                </div>
            </div>
        )
    }
}


