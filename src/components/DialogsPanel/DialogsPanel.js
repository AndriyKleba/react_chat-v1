import React from "react";
import DialogsList from "../Dialogs";
import InputSearch from "../InputSearch";

import "./DialogsPanel.css";

export default class DialogsPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [...this.props.items],
            filterText: ''
        }

    }

    onSearchChange = (filterText) => {
        this.setState({
            filterText
        })
    }

    searchDialog(items, text) {
        const newItems = [...items]
        if (text.length === 0) {
            return newItems
        }

        return newItems.filter(item => {
            return item.user.fullName
                .toLowerCase()
                .indexOf(text.toLowerCase()) > -1
        })
    }


    render() {
        const {items, filterText} = this.state
        const currentSearchElement = this.searchDialog(items, filterText)
        const {selectDialog} = this.props

        return (
            <div className="chat__dialogs-sidebar">

                <div className="chat__dialogs-header">

                    <div className="chat__dialogs-header--avatar">
                        <img src="https://howsmydealing.com/wp-content/uploads/2016/12/anonymous-icon.jpg"
                             alt="Anonim Icon"/>
                    </div>

                    <div className="chat__dialogs-search">
                        <InputSearch onSearchChange={this.onSearchChange}/>
                    </div>
                </div>

                <div className="chat__dialogs-list-item">

                    <div className="chat__dialogs-list-item-title">
                        <h1>Chats</h1>
                    </div>


                    <div className="chat__dialogs-list-item-messages">

                        <DialogsList
                            items={currentSearchElement}
                            onSelectDialog={selectDialog}
                        />

                    </div>


                </div>

            </div>
        )
    }
}

