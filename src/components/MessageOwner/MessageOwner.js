import React from "react";
import Time from "../Time";

import './MessageOwner.css'

export default class Message extends React.Component {

    render() {
        const {avatar, created_at, value} = this.props

        return (
            <div className="messageIsMe">
                <div className="messageIsMe__avatar">
                    <img src={avatar} alt="User Avatar"/>
                </div>
                <div className="messageIsMe__content">
                    <div className="messageIsMe__information">
                        <p className="messageIsMe__text">{value}</p>
                    </div>
                    <span className="messageIsMe__date">{<Time date={created_at}/>}</span>
                </div>
            </div>
        )
    }
}


