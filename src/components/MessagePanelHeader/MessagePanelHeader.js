import React from "react";

import "./MessagePanelHeader.css";
import AvatarUser from "../AvatarUser";

export default class MessagePanelHeader extends React.Component {


    render() {
        const userInfo = {...this.props.info.infoUser}

        return userInfo.fullName === undefined ?
            (<div className="chat__dialogs-header-no_info">
                <span>Please select person to start chat</span>
            </div>)
            :
            (<div className="chat__main-dialog-header">
                <div className={`chat__main-dialog-header--avatar`}>
                    <AvatarUser user={userInfo}/>
                    <b className="chat__main-dialog-header--username">{userInfo.fullName || 'Please select Item'}</b>
                </div>
            </div>)
    }
}
