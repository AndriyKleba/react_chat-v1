import React from "react";
import format from 'date-fns/format';
import isToday from 'date-fns/isToday';
import AvatarUser from "../AvatarUser";

import './DialogItem.css';

export default class DialogItem extends React.Component {

    state = {
        username: this.props.item.user.fullName
    }

    getMessageTimeFormat = date => {
        if (isToday(date)) {
            return format(new Date(date), 'HH:mm')
        }
        return format(new Date(date), 'MMM, dd yyyy')
    }


    currentItem = (id) => {
        this.props.SelectDialog(id)
        // console.log(id)
        // console.log('dsds', this.state.username)
        // const item = document.getElementById('activeElement')
        // console.log(item.childNodes)
        // if (id === this.state.username) {
        //     item.classList.add('active')
        // }


    }


    render() {
        const {id, user: {fullName}, message: {lastMessage, lastDialogDate}} = this.props.item


        return (
            <div id="activeElement" className="dialogs__item " onClick={() => this.currentItem(id)}>
                <AvatarUser user={this.props.item.user}/>
                <div className="dialogs__item-info">
                    <div className="dialogs__item-info-top">
                        <b>{fullName}</b>
                        <span>
                            {this.getMessageTimeFormat(lastDialogDate)}
                        </span>
                    </div>
                    <div className="dialogs__item-info-bottom">
                        <p>{lastMessage}</p>
                    </div>
                </div>
            </div>
        )
    }
}