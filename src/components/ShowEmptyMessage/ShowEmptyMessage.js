import React from "react";

import "./ShowEmptyMessage.css";

export default class ShowEmptyMessage extends React.Component {

    render() {
        return (
            <div className="banner__message-empty">
                <h1>Contacts not found</h1>
                <span className="banner__message-empty-img"/>
            </div>
        )
    }
}