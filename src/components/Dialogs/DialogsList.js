import React from "react";
import DialogItem from "../DialogItem";
import ShowEmptyMessage from "../ShowEmptyMessage";

import './DialogsList.css';

export default class DialogsList extends React.Component {



    render() {
        const {items, onSelectDialog} = this.props
        return (
            <div className="dialogs">
                {
                    items.length ? items.map(item =>
                            (
                                <DialogItem
                                    key={item.id}
                                    item={item}
                                    SelectDialog={onSelectDialog}
                                />
                            )
                        )

                        :

                        <ShowEmptyMessage/>
                }
            </div>
        )
    }
}