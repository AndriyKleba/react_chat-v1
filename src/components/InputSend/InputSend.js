import React from "react";

import "./InputSend.css";

export default class InputSend extends React.Component {

    state = {
        valueOwner: '',
    }

    onTypeMessage = (event) => {
        this.setState({
            valueOwner: event.target.value
        })

    }

    answerMessages = () => {
        let messOwner = document.getElementById('messageInput')
        if (messOwner.value === '') {
            return
        }
        this.props.onMessageAddOwner(this.state.valueOwner)
        this.props.onMessagePartner()
        messOwner.value = ''
    }


    render() {
        const userInfo = {...this.props.info.infoUser}

        return (
            userInfo.fullName === undefined ? null : <div className="chat__main-dialog-message--text">
                <div className="input__send-chat">
                    <form className="input__send-message" action="#" id="send">
                        <input placeholder="Type your message" id="messageInput" onChange={this.onTypeMessage}/>
                        <button className="input__send-message--button" onClick={this.answerMessages}/>
                    </form>
                </div>
            </div>
        )
    }
}