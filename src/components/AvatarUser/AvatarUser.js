import React from "react";

import "./AvatarUser.css";

export default class AvatarUser extends React.Component {

    render() {
        const {avatar, fullName, isOnline} = this.props.user


        const userOnline = isOnline ? 'dialogs__item--online' : '';

        return (
            <div className={`dialogs__item-avatar ${userOnline}`}>
                <img src={avatar} alt={`User ${fullName}`}/>
            </div>
        )
    }
}