import React from 'react';
import InputSend from "../InputSend";
import DialogsPanel from "../DialogsPanel";
import MessagePanel from "../MessagePanel";
import MessagePanelHeader from "../MessagePanelHeader";
import ChuckNorrisService from "../../services/ChuckNorrisService";

import './App.css';


export default class App extends React.Component {
    chuck = new ChuckNorrisService()

    isMyDialogs = 100
    isPartnerDialogs = 100


    state = {
        items: [
            {
                id: 1,
                user: {
                    fullName: 'Alice Freeman',
                    avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTC2rSz73LjrP1igpamM7aulHN3Sz3STdtPB_Ojp7ycfkl2eIrW&usqp=CAU",
                    isOnline: true,
                },
                message: {
                    lastMessage: "You are the worst!",
                    lastDialogDate: new Date(),
                },
                dialogs: {
                    id: 1,
                    messages: []
                }
            },
            {
                id: 2,
                user: {
                    fullName: 'Josefina',
                    avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTV3H-uAPE7BKbYUGa42fKw6rTd-rYy8Tj7FqsE8OMDDIcLj3_k&usqp=CAU",
                    isOnline: false,
                },
                message: {
                    lastMessage: "We are losing money! Quick!",
                    lastDialogDate: new Date(),
                },
                dialogs: {
                    id: 2,
                    messages: []
                }
            },
            {
                id: 3,
                user: {
                    fullName: 'Valazquez',
                    avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9dtrUplQ36YS_eDinUcE5j2kIwCn8rrh25PX9YUNd_Vbxb0Zk&usqp=CAU",
                    isOnline: true,
                },
                message: {
                    lastMessage: "Quickly come to the meeting room 1B, we have a big server issue",
                    lastDialogDate: new Date(),
                },
                dialogs: {
                    id: 3,
                    messages: []
                }
            },
            {
                id: 4,
                user: {
                    fullName: 'Barrera',
                    avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTcs8BJ3-xcpbDFuf_AJlwxuEW82Y4nO1rUdhvQD4bgQrUfdNme&usqp=CAU",
                    isOnline: false,
                },
                message: {
                    lastMessage: "Hello, give you plan!",
                    lastDialogDate: new Date(),
                },
                dialogs: {
                    id: 4,
                    messages: []
                }
            }
        ],
        dialogs: [],
        headerItem: {}
    }


    addMessageOwner = (text) => {
        const newMessage = {
            id: this.isMyDialogs++,
            isMe: true,
            avatar: "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
            created_at: new Date(),
            updated_at: new Date(),
            value: text
        }
        console.log(newMessage.id)
        this.setState(({dialogs}) => {
            const newDialogs = [...dialogs, newMessage]
            return {
                dialogs: newDialogs
            }
        })
    }


    addMessagePartner = async () => {
        await this.chuck.getChuckNorris()
            .then(res => res)
            .then(data => {
                const newMessage = {
                    id: this.isPartnerDialogs++,
                    isMe: false,
                    avatar: "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                    created_at: new Date(),
                    updated_at: new Date(),
                    value: data.value
                }

                this.setState(({dialogs}) => {
                    const newDialogs = [...dialogs, newMessage]
                    return {
                        dialogs: newDialogs
                    }
                })
            }).catch(err => alert(`Something problem ${err} resend your message`)
            )
    }

    selectHeaderPartner = (id) => {
        const item = this.state.items.filter(item => id === item.id ? item : null)
        const infoUser = {...item[0].user}
        this.setState(({headerItem}) => {
            const info = {...headerItem, infoUser}
            return {
                headerItem: info
            }
        })
    }


    onSelectDialog = (id) => {
        this.selectHeaderPartner(id)
    }

   addMessageToItem = () => {

   }

    render() {
        return (
            <section className="home">

                <div className="chat">

                    <DialogsPanel
                        items={this.state.items}
                        selectDialog={this.onSelectDialog}
                    />

                    <div className="chat__main-dialog">

                        <MessagePanelHeader
                            info={this.state.headerItem}
                        />


                        {/*<MessagePanel items={this.state.items} dialogs={this.state.dialogs}/>*/}
                        <MessagePanel
                            dialogs={this.state.dialogs}
                            items={this.state.items}
                        />


                        <InputSend
                            onMessageAddOwner={this.addMessageOwner}
                            onMessagePartner={this.addMessagePartner}
                            info={this.state.headerItem}
                        />

                    </div>

                    <div className="chat__footer"/>

                </div>

            </section>
        )
    }
}