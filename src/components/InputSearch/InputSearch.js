import React from "react";

import "./InputSearch.css";

export default class InputSearch extends React.Component {

    state = {
        filterText: ''
    }

    searchFilterName = e => {
        const filterText = e.target.value
        this.setState({filterText})
        this.props.onSearchChange(filterText)
    }

    render() {
        return (
            <div className="search__item">
                <input
                    value={this.state.filterText}
                    onChange={this.searchFilterName}
                    type="text"
                    name="search"
                    placeholder="Search or start new chat"/>
            </div>
        )
    }
}
